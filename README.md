# コーディングガイドライン #

## ディレクトリ構成 ##

### プロジェクトディレクトリ構成 ###
```
/root
  /gulp              // gulpタスクディレクトリ
  /src               // 開発用ディレクトリ
  /public            // 公開用ディレクトリ
  // 以下各種設定ファイル
  .editorconfig
  .gitignore
  .htmlhintrc
  .npmrc
  .nvmrc
  babel.config.js
  gulpfile.js
  package.json
  postcss.config.js
  webpack.config.js
  yarn.lock
```



### ソースディレクトリ構成 ###
```
/src
  /includes    // サイト共通モジュールファイル格納用ディレクトリ（ヘッダー等）
  /data        // ejsにデータを流し込む際のjsonファイル格納用ディレクトリ
  /assets      // サイトで使用するリソース格納用ディレクトリ
    /fonts        // フォントファイル格納用ディレクトリ
    /images       // 画像ファイル格納用ディレクトリ
    /scripts      // JavaScript格納用ディレクトリ
    /styles       // スタイルシート格納用ディレクトリ
      /abstracts      // sass共通設定ファイルディレクトリ(mixin, function, 変数)
      /base           // ベーススタイルの定義
      /components     // コンポーネント定義
      /layouts        // レイアウトの定義
      /utilities      // clearfix等のユーティリティスタイルの定義
      style.scss      // サイト共通スタイルシート（上記ディレクトリのスタイルシートを展開）
      /dir01              // ユニークページディレクトリ
        _sample01.scss    // ページ内の任意の単位のスタイル
        _sample02.scss
        _sample03.scss
        index.scss        // ページ専用スタイルシート（上記アンダースコア付きStyleSheetを展開）
  index.ejs
  /dir01
    index.ejs
```



### サポートブラウザ ###

#### Desktop ####
- Windows
    - Internet Explorer 11 (latest version)
    - Microsoft Edge (latest version)
    - Google Chrome (latest version)
    - Mozilla Firefox (latest version)
- Mac
    - Google Chrome (latest version)
    - Mozilla Firefox (latest version)
    - Safari (latest version)

#### Mobile ####
- Mobile Safari (latest version)
- Mobile Chrome (latest version)

#### Mobile OS version ####
- iOS (latest version) 2018/03/01現在
- Android 5.0.2+


## HTML ##

### ドキュメントタイプ ###

`HTML5` を基本とします。
```html
<!DOCTYPE html>
```


### 文字コード ###

`UTF-8` を基本とします。

```html
<meta charset="UTF-8">
```


### title ###

以下のようにパイプで階層を区切って記述します。

```html
<title>＜ページ名＞｜＜カテゴリ名＞｜＜サイト名＞</title>
```


### meta ###

#### meta:keyword ####

キーワードは重要なものから順に記述します。

```html
<meta name="keywords" content="キーワード1,キーワード2,キーワード3,キーワード4,キーワード5">
```

#### meta:description ####

ページの概要を検索エンジンロボットに認識させます。
1文か2文、場合によっては短い段落程度の文章でページの概要を入力します。
具体的に、120字~200字前後に収めるように記述します。

```html
<title>＜ページ名＞｜＜カテゴリ名＞｜＜サイト名＞</title>
```


### SNS対応 ###

```html
<meta property="fb:app_id" content="123456789">
<meta property="og:url" content="https://example.com/page.html">
<meta property="og:type" content="website">
<meta property="og:title" content="Content Title">
<meta property="og:image" content="https://example.com/image.jpg">
<meta property="og:description" content="Description Here">
<meta property="og:site_name" content="Site Name">
<meta property="og:locale" content="ja_JP">
<meta property="article:author" content="">
```

|Property|Description|Required|
|:---|:---|:---:|
|fb:app_id|指定しておくと `*Facebookインサイト` が使えます。（Facebookインサイトについては下記参照）|-|
|og:url|シェアするURLを記述します。シェアしたいURLを絶対パスで記述します。指定無しの場合は自動取得されます。|*|
|og:type|サイトタイプを記述します。ドメインルート配下のみウェブサイトには `website` 、ブログには `block` と記述。<br>下層に関しては `article` と記述します。<br>[Open Graph Reference Documentation](https://developers.facebook.com/docs/reference/opengraph/)|*|
|og:title|ページタイトルを記述します。 `meta:title` と同じものを記述します。|*|
|og:image|シェア内に表示される画像。シェア時に表示される画像を絶対パスで記述します。<br>最低でも 600 x 315 px、また大きければ大きいほど良いため 1200 x 630 px 以上の画像サイズが推奨です。<br>[OGP画像シミュレータ](http://ogimage.tsmallfield.com/)|*|
|og:description|ページの説明を記述します。指定しない場合は `meta:description` が反映されます。|-|
|og:site_name|サイト名を記述します。|-|
|og:locale|サポート言語を記述します。<br>日本語なら `ja_JP` , 複数言語なら `property属性` を `og:locale:alternate` に設定し `content属性` に言語ごとに記述します。|-|
|article|Facebookページの紐付けを記述します。Facebookページを紐付けるなら `publisher` 、個人アカウントを紐付けるなら `author` を記述します。 `content属性` には該当のFBページURLを記述します。|-|
|twitter:card|Twitterでのシェアのコンテンツの指定を記述します。<br> `summary` , `summary_large_image` , `photo` , `gallery`|-|
|twitter:site|カードのフッター部分にTwitterID名が表示されます。|-|

** Facebookインサイト **
設定したサイトのいいねやシェアなどのアクション数をグラフで確認することができて、Facebookで集客を考えている方には必須のツールです。 [Facebookインサイト設置方法](http://www.amamoba.com/twitter/fb-insight.html)


### favicon, WebClip ###

```html
<link rel="icon" href="apple-touch-icon.png">
<link rel="shortcut icon" href="apple-touch-icon.png">
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
<link rel="apple-touch-icon" href="apple-touch-icon.png">
<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
```


### 表記 ###

#### インデント ####

インデントは `2 spaces` で記述する。

#### コメントアウト ####

div要素の終了タグ直前には下記のようにコメントを挿入し終了位置を明確にします。

```html
<div class="content">
	<section class="section">
		<h1>ガイドライン</h1>
		<p>コーディングガイドラインに準拠してください。</p>
	</section><!-- .section  -->
</div><!-- .content  -->
```

#### 特殊文字・機種依存文字 ####

|表示|説明|ソース|
|:---|:---|:---|
|&amp;|アンパサンド|`&amp;`|
|&lt;|比較記号|`&lt;`|
|&gt;|比較記号|`&gt;`|
|&quot;|引用符|`&quot;`|
|&yen;|円|`&yen;`|
|&copy;|コピーライト|`&copy;`|
|&reg;|登録商標|`&reg;`|


### id / class名 ###

- 名称に関しては分かりやすい英単語を用いる。
- 単語間の区切りには `ハイフン(-)`のみ使用可。(キャメルケース、スネークケースは禁止)
- サイト全体で使う共通パーツにはレイヤーの接頭辞を付け（下記参照）、ページユニークCSSには接頭辞を付けない
- セレクタの命名規則は `BEM` を使用


### 参考資料 ###

- [MindBEMding](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)
- [きちんとしたHTML/CSSを記述するために参考になるスライド8選](http://uxmilk.jp/46031)


### BEMのセパレータについて ###

- Elementは `アンダースコア2つ(__)`
- Modifierは `ハイフン2つ(--)`

|Layer|Separater|Example|
|:----|:----:|:----|
|Block|-|-|
|Element| `__` | `Block__Element` |
|Modifier| `--` | `Block--Modifier` , `Block__Element--Modifier` |


### 設計手法・プリフィクスについて ###

[SMACSS](https://app.codegrid.net/entry/smacss-1)をベースに下記のレイヤーとする。
- Base
- Layer
- Component
- Module
- Utility
- State
- js-trigger

|Layer||Prefix|Example|
|:----|:---|:----|:----|
|Base|タイプセレクタによるプロジェクト全体のスタイルを定義|-|-|
|Layout|レイアウトに関する要素のスタイルを定義|`.l-*`|`.l-wrap`|
|Component|最小のモジュールのスタイルを定義|`.c-*`|`.c-btn`|
|Module|より具体的なモジュールを定義（コンポーネントの集合）|`.m-*`|`.m-card`|
|Utility|ユーティリティスタイルを定義|`.u-*`|`.u-img-flex`|
|State|状態のスタイルを定義（JavaScriptによって制御される）|`.is-*`|`.is-active`|
|js-trigger|JavaScriptでトリガーになる要素に付与（スタイル無し）|`.js-*`|`.js-modal-trigger`|


## CSS ##

### CSSプリプロセッサについて ###

- CSSプリプロセッサは `Sass` を使用。
- Sassの記述方式は `SCSS方式` を使用。


### 表記 ###

front-end-templateのLinterの設定に準拠。

### コメント ###
```
/*==========================================================================

   x-large

===========================================================================*/

/*--------------------------------------------------------------------------
   large
---------------------------------------------------------------------------*/

/* middle
-----------------------------------------------------------------*/

/* small */

/* media query -> sp ( or pc )
=================================================================*/
```


## JavaScript ##

基本的には、ECMAScript2015以降の記法で記述する。

### クオート ###

クオートは一貫性を持たせる為、シングルクウォートを優先して使用する。
```js
/**
 * bad
 */
var vogaro = "vogaro";


/**
 * good
 */
var vogaro = 'vogaro';
```


### タブインデント・空白 ###

front-end-templateのLinterの設定に準拠。


### 改行 ###

コロン、セミコロン前での改行はしない。
1行は80文字以内を目安に改行し、1行が長くなる場合は、適度な個所で改行する。 ※任意
ファイル最終行は、1行改行を入れる。

```js
/**
 * bad
 */
// コロン、セミコロン前での改行
var vogaro = ['tokyo'
, 'osaka']
;

// 1行は80文字以上
$('.vogaro').animate({opacity: 0}).animate({opacity: 1}).animate({opacity: 0}).animate({opacity: 1}).animate({opacity: 0});


/**
 * good
 */
// コロン、セミコロン前で改行しない
var vogaro = ['tokyo', 'osaka'];

// 1行は80文字以内(メソッドチェーンの改行はインデントする)
$('.vogaro').animate({opacity: 0})
  .animate({opacity: 1})
  .animate({opacity: 0})
  .animate({opacity: 1})
  .animate({opacity: 0});
```


### カンマ ###

先頭のカンマの使用禁止

```js
/**
 * bad
 */
var vogaro
  , tokyo
  , osaka;

var vogaro = {
  tokyo: 10
  , osaka: 10
};


/**
 * good
 */
var vogaro,
    tokyo,
    osaka;

var vogaro = {
  tokyo: 10,
  osaka: 10
};
```


### セミコロン ###

セミコロンは記述しない。

```js
/**
 * bad
 */
(function () {
  alert(0)
}())

var vogaro = ['tokyo', 'osaka']


/**
 * good
 */
(function () {
  alert(0);
}());

var vogaro = ['tokyo', 'osaka'];
```


### 予約語 ###

変数、関数、メソッド、オブジェクトの識別子として予約語は使用しない。

- [予約語 - JavaScript | MDN](https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Reserved_Words)

```
// 予約語リスト
break
case
catch
continue
debugger
default
delete
do
else
finally
for
function
if
in
instanceof
new
return
switch
this
throw
try
typeof
var
void
while
with

// 将来の使用を見越した予約語
class
enum
export
extends
import
super
implements
interface
let
package
private
protected
public
static
yield
```

### Object (オブジェクト) ###

オブジェクトを作成する際は、リテラル構文を使用する。

```js
/**
 * bad
 */
var vogaro = new Object();

// 予約語をキーとして使用しない
var vogaro = {
  default: {
    class: 'tokyo'
  },
  private: true
};


/**
 * good
 */
var vogaro = {};

// 予約語の代わりに分かりやすい同義語を使用
var vogaro = {
  defaults: {
    type: 'tokyo'
  },
  hidden: true
};
```


### Array (配列) ###

配列を作成する際は、リテラル構文を使用する。
配列の変数名は、複数形を使う。

```js
/**
 * bad
 */
var items = new Array();

// 変数名が複数形になっていない
var item = [];


/**
 * good
 */
var items = [];
```


### 型 ###

変数の型変換は極力避ける。
また変数は、型、値が分かり易いように命名する。

```js
/**
 * bad
 */
// 配列型から文字列型に変換されている。
var vogaro = [];

if ( flag ) {
  vogaro = 'tokyo';
} else {
  vogaro = 'osaka';
};


/**
 * good
 */
var vogaro;

if ( flag ) {
  vogaro = 'tokyo';
} else {
  vogaro = 'osaka';
};
```


### 変数・グローバルネームスペース ###

変数を宣言する際は、常に var を使用する。(宣言を忘れるとグローバルネームスペースが汚れる。)
グローバルネームスペースを汚さない為に、グローバルネームスペース使用は最小限にする。
グローバルネームスペースに追加する際は、ネームスペースが衝突しないように配慮して追加する。
ファイル内のコードは、グローバルネームスペースを汚さないように即時関数で、ラップする。

```js
/**
 * bad
 */
// varの変数宣言していない
vogaro = [];

// グローバルネームスペースを多く使い汚している
window.vogaro = {};
window.tokyo = {};
window.osaka = {};

// グローバルネームスペースの有無を確認せずに追加している
window.vogaro = {};

// ファイルのコードが即時関数でラップされていない
common.js
  var common = {};
  // some code...


/**
 * good
 */
var vogaro = [];

// グローバルネームスペースを1つにまとめる
window.vogaro = {
  tokyo： {},
  osaka: {}
};

// グローバルネームスペースの有れば、既存のグローバルネームスペースを使用、無ければ追加
window.vogaro = window.vogaro || {};

// ファイルのコードが即時関数でラップする
// common.js
(function () {
var common = {};
	// some code...
}());
```


### 命名規則 ###

変数名、関数名の命名は省略しすぎない。
変数名は型、値が分かり易い名前をつける。
関数名は処理が分かり易い名前を付ける。
thisを変数に代入する際は、 self やthisが何を指すのか分かり易い名前にする。

```js
/**
 * bad
 */
// 複数形で記述されていない
var item = [];

// 名前と型が一致しない
var item = false;

// 処理が分かりにくい
item('pen');

// 省略しすぎて分かりにくい
var t =  this;


/**
 * good
 */
// 型、値が分かり易い名前をつける
// 例）
var items = [];
var current = 0;
var isShow = false;
var fullName = 'vogaro osaka';
var config = {};
var self =  this;

// 処理が見え易い関数名をつける
// 例）
getItem('pen'); // 値の取得
setItem('pen'); // 値のセット
hasItem('pen'); // bool値を返す
```


### 記法 ###
```js
・グローバルネームスペース： ローワーキャメル（アッパーケース非推奨）
グローバルネームスペースは、バッティングを避ける為、できるだけ固有名詞をつけてください。
window.vogaro = window.vogaro || {};
window.common = window.common || {};


・ローカル変数： ローワーキャメル
var vogaro = {};
var vogaro = function () {};

・定数： アッパーケース + スネークケース（接続詞はアンダーバー）
var IN_VOGUE = 10;

・コンストラクタ： パスカルケース
var vogaro = function () {};


■ プロトタイプ設計のアクセス修飾子
・パブリック : 上記記法(ローカル変数・定数)(どこからでも呼び出せる)
・スタティック : 上記記法(ローカル変数・定数)(静的なメソッド・プロパティ)
・プライベート： 先頭アンダーバー + 上記記法(ローカル変数・定数)(外部からアクセスされたくない)
・プロテクト : 先頭アンダーバー + 上記記法(ローカル変数・定数)（継承したプロトタイプで使用可）


/**
 * サンプル
 *
 * @constructor
 * @class Sample
 */
var Sample = function () {};

/**
 * 社名をアラート
 *
 * @public
 * @method callName
 * @return {Void}
 */
Sample.prototype.callName = function () {
  alert(Sample.name);
};

/**
 * 社名
 *
 * @static
 * @property name
 * @type {String}
 */
Sample.name = 'vogaro';

/**
 * voraroメンバーへのメールアドレス
 *
 * @static
 * @private
 * @property _ALL_EMAIL
 * @type {String}
 */
Sample._ALL_EMAIL = 'all@vogaro.xx.xx';

/**
 * プロトタイプを拡張します
 *
 * @protected
 * @static
 * @method _extend
 * @return {ExtendClass}
 */
Sample._extend = function () {
  // somecode...
}
```


### 条件式と等価式 ###

条件式は、可読性を意識して、見やすい式で記述してください。
処理が複雑なときは、コメントを残してください。

```js
// TIPS
{}, [], function () {} 等のオブジェクトは true と評価されます。
undefined, null は false と評価されます。
真偽値 は boolean型の値 として評価されます。
数値 は true と評価されます。 +0, -0, NaN の場合は false です。
文字列 は true と評価されます。 空文字 '' の場合は false です。


/*--------------------------------------------------*/


/**
 * bad
 */
if (name !== '') {
  // some code
}

// 視認性、可読性が悪い、処理が分かりにくい
if ( flag && $(window).scrollTop() < offsetY[0] && $(window).scrollTop() > offsetY[1] ) {
  // some code
}


/**
 * good
 */

if ( name ) {
  // some code
}

// 可読性を意識して、分かり易い変数に代入する
var scrollY = $(window).scrollTop();

// 分かりにくいときは、処理の条件をコメントで残す
if ( flag && scrollY < offsetY[0] && scrollY > offsetY[1] ) {
  // some code
}
```


### ブロック ###

複数行のブロックには中括弧{}を使用してください。

```js
/**
 * bad
 */
// ブロック使用していない
if ( test )
  return false;

// ブロックで改行できていない
function () { return false; }


/**
 * good
 */
if ( test ) return false;

if ( test ) {
  return false;
}

function () {
  return false;
}
```


### ネスト ###

ネスト構造は深くなりすぎないようにする。（3階層くらいを目安に）

```js
/**
 * bad
 */
// ネストが深すぎて可読性が低い（処理を見直す必要がある）
if ( flag01 ) {
  if ( flag02 ) {
    if ( flag03 ) {
      if ( flag04 ) {
      // some code
      }
    } else {
      // some code
    }
  } else {
    // some code
  }
}

// コールバックヘル（処理が追えない。メンテナンスしにくい。）
setTimeout(function () {
  alert(0);
  setTimeout(function () {
    alert(1);
    setTimeout(function () {
      alert(2);
    }, 3000);
  }, 2000);
}, 1000);


/**
 * good
 */
if(flag01){
  if(flag02){
    // some code
  } else {
    // some code
  }
}

var timer = {
  init: function () {
    setTimeout(timer.scene01, 1000);
  },
  scene01: function () {
    alert(0);
    setTimeout(timer.scene02, 2000);
  },
  scene02: function () {
    alert(1);
    setTimeout(timer.scene03, 3000);
  },
  scene03: function () {
    alert(2);
  }
};

timer.init();
```


### コメント ###

機能ごとに区切ってか視認性を高める為、必要に応じて「区切りコメント」を入れる。（※出来るだけ入れてください。）
プラグイン、コンストラクタ、モジュール等の関数に概要がわかるブロックコメントを入れる。
処理が複雑な部分や、重要な部分には1行コメントを入れる。
コメントアウト前には、改行を入れ可読性をよくする。
※極力コメントパターンルールは守る。
コメントパターンは出来れば[YUIDoc](http://yui.github.io/yuidoc/syntax/index.html)にあわせてください。

```js
/**
 * bad
 */
// 関数概要コメントがない
var slider = function(){
  // some code...
};

// コメントアウトがなく処理がわかりにくい
var vogaro = function () {
  if ( vogaro.top < vogaro.bottom ) {
    // some code...
  }
};


/**
 * good
 */
/**
 * メインビジュアルのスライダー
 *
 * @class slider
 * @param  {jQuery} $target スライダーのラッパーエレメント
 * @param  {Object} options オプション値
 * @return {Instance} インスタンスを返す
 */
var slider = function () {
  // some code...
};


var vogaro = function () {

  // スクロール値がメインビジュアルのオフセット値より大きい場合
  if ( vogaro.top < vogaro.bottom ) {
    // some code...
  }
};
```


### コメントパターン ###

```js
・基本は以下3パターンを使用して下さい。


1.区切りコメント： モジュール、プラグイン等を区切る際に使用してください。(区切ることで可読性を上げる為です)
/*----------------------------------------------------------------------
  プラグイン名を入れてください
----------------------------------------------------------------------*/


2.ブロックコメント: モジュール、プラグインの概要をまとめたものをコメントしてください。
（簡易的なものでも構いませんのでブロックコメントは心がける）
/**
 * slider スライダー
 *
 * @method slider
 * @param  {jQuery} $target スライダーラッパー要素
 * @param  {Object} options オプション値
 * @return {Instance} sliderインスタンスを返す
 */


（簡易版）
/**
 * スライダー
 */


3.処理が複雑な箇所や、一時的な処理等を記述する。
// 処理概要を記述してください。
```

### コンストラクタ ###

コンストラクタにはパスカルケース（大文字から始まる）を使用する。
プロトタイプは、新しいオブジェクトでプロトタイプをオーバーライドするのではなく、プロトタイプオブジェクトにメソッドを追加する。プロトタイプをオーバーライドすると継承が不可能になる為。

```js
/**
 * bad
 */
var vogaro = function () {
  this.init();
};

// プロトタイプをオーバーライドしている
vogaro.prototype = {
  init: function () {
    this.set();
  },

  set: function () {
    // some code
  }
};


/**
 * good
 */
var vogaro = function () {
  this.init();
};

var proto = vogaro.prototype;

proto.init = function () {
  // some code
};

proto.set = function () {
  // some code
};

// もしくは
vogaro.prototype.init = function () {
  // some code
};

vogaro.prototype.set = function () {
  // some code
};
```


### jQuery ###

jQueryオブジェクトの変数は、先頭に $ を付与する。
jQueryを使用するファイルは、$のネームスペースがバッティングしないように、無名関数でラップする。
jQueryのネームスペースは、むやみに汚さない。追加する場合は、把握した上で追加すること。jQueryのデフォルトメソッドをオバーライドしないようにするため。

```js
/**
 * bad
 */
var vogaro = $('.vogaro');

// jQueryのネームスペースはむやみに汚さない。
$.slider = function () {
  // some code
};
$.tab = function () {
  // some code
};


/**
 * good
 */
var $vogaro = $('.vogaro');

// jQueryに追加する必要が無ければ、他のオブジェクトにする
var vogaro = {};

vogaro.slider = function () {
  // some code
};

vogaro.tab = function () {
  // some code
};
```


### ファイル ###

ファイルの先頭は、; （セミコロン）を入れる。※ 推奨
（文末のセミコロンを付け忘れたモジュールを連結した場合、実行時にエラーが発生しない為）
ファイル名は、モジュール名、プラグイン名、役割が分かる名前をつける。
ファイル名は、大文字を使用しない。
※ jQueryを使用する場合は、$のネームスペースがバッティングしないように、無名関数でラップしてください。

```js
// ファイルの先頭に ; （セミコロン）。
;var vogaro = vogaro || {};

(function ($) {
  /**
   * slider
   */
  vogaro.slider = function () {
    // some code
  };

}(jQuery));

// ファイル名
vogaro.slider.js
```


### セキュリティ ###

ユーザーが文字列入力して動作する関数は、XSS対策に配慮する。
eval関数は、使わない。

- [XSSの傾向と対策](https://app.codegrid.net/entry/security-xss)
- [jQuery1.8でXSS対策に.parseHTMLメソッドが追加](http://blog.sarabande.jp/post/30740431936)
- [eval関数](http://splitchin.com/tech/2011/05/29/eval%E4%BD%BF%E7%94%A8%E3%81%97%E3%81%AA%E3%81%84%E3%80%82-javascript%E3%83%91%E3%82%BF%E3%83%BC%E3%83%B3/)
- [クロスサイトスクリプティング脆弱性とは？](http://www.atmarkit.co.jp/ait/articles/0211/09/news002.html)
- [JSON セキュリティ対策](http://www.websec-room.com/2013/07/28/876)
